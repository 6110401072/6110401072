package filesCode;

import java.io.*;

public class FileUser {
    private String firstName;
    private String lastName;
    private String eMail;
    private String userName;
    private String password;

    private File file = new File("files/userDetails.csv");

    public FileUser(String firstName, String lastName, String eMail, String userName, String password){
        this.firstName = firstName;
        this.lastName = lastName;
        this.eMail = eMail;
        this.userName = userName;
        this.password = password;
    }

    public FileUser(String username, String password) {
        this.userName = username;
        this.password = password;
    }

    public void save(){
        FileWriter writer;
        try {
            writer = new FileWriter(file,true);
            writer.write( firstName + "," + lastName + "," + eMail + "," + userName + "," + password + "\n");
            writer.close();
        } catch (IOException e) {
            System.out.println("File " + " not found");
            e.printStackTrace();
        }
    }

    public boolean registerCheck(){
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String line;
            while ((line = reader.readLine()) != null){
                String[] arrUser = line.split(",");
                if (arrUser[3].equals(userName)) {
                    return true;
                }
            }
        }catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("can't read file");
            e.printStackTrace();
        }
        return false;
    }

    public int checkUserAndPass(){
        FileReader fileReader;
        try {
            fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String line;
            while ((line = reader.readLine()) != null){
                String[] arrUser = line.split(",");
                if (arrUser[3].equals(userName)) {
                    if (arrUser[4].equals(password)) {
                        return 1;
                    }
                    return 2;
                } else if (arrUser[4].equals(password)){
                    return 3;
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("can't read file");
            e.printStackTrace();
        }
        return 4;
    }

}