package filesCode;

import theatre.TmpData;

import java.io.*;
import java.util.List;

public class FileBookingData {
    private File file = new File("files/BookingData.csv");

    private TmpData tmpData;

    public FileBookingData(TmpData tmpData) {
        this.tmpData = tmpData;
    }

    public void save(){
        FileWriter writer;
        try {
            writer = new FileWriter(file, true);
            writer.write(tmpData.getUserName() + "," + tmpData.getMovieName() + "," + tmpData.getTime() + "," + tmpData.getTheatreName() + "," + saveChair(tmpData.getArrListPositionChairI()) + "," + saveChair(tmpData.getArrListPositionChairJ()) + "\n");
            writer.close();
        } catch (IOException e) {
            System.out.println("Can't find BookingData.csv");
            e.printStackTrace();
        }

        readBooking();
    }

    private String saveChair(List<Integer> arrListPositionChairI) {
        String chair = "";
        for (Integer integer : arrListPositionChairI) {
            chair += integer + ":";
        }
        return chair;
    }

    public void readBooking() {
        String line;
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            while ((line = reader.readLine()) != null){
                String[] arrChair = line.split(",");
                if (arrChair[1].equals(tmpData.getMovieName())){
                    if (arrChair[2].equals(tmpData.getTime())){
                        String[] arrI = arrChair[4].split(":");
                        String[] arrJ = arrChair[5].split(":");
                        int i = 0;
                        for (String s : arrI) {
                            tmpData.addOldPositionChairI(Integer.parseInt(s.trim()));
                            tmpData.addOldPositionChairJ(Integer.parseInt(arrJ[i].trim()));
                            i++;
                        }
                    }
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public TmpData getTmpData() {
        return tmpData;
    }
}
