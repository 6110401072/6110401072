package theatre;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import theatre.home.LoginController;
import theatre.selectTheatre.SelectTheatreController;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import theatre.movie.SetMovie;
import theatre.movie.Movie;

import javafx.fxml.FXML;
import javafx.scene.image.ImageView;

import java.io.IOException;

public class SelectMovieController {
    protected TmpData tmpData = new TmpData();
    private SetMovie setMovie = new SetMovie();
    private Movie[] m = setMovie.getM();

    @FXML
    ImageView m1ImageView, m2ImageView, m3ImageView, m4ImageView, m5ImageView, m6ImageView, imageViewBg;
    @FXML
    Button btnLogout, btnLogin;

    @FXML public void initialize(){
            Platform.runLater(() -> {
                m1ImageView.setImage(new Image(m[0].getImage()));
                m2ImageView.setImage(new Image(m[1].getImage()));
                m3ImageView.setImage(new Image(m[2].getImage()));
                m4ImageView.setImage(new Image(m[3].getImage()));
                m5ImageView.setImage(new Image(m[4].getImage()));
                m6ImageView.setImage(new Image(m[5].getImage()));
                imageViewBg.setImage(new Image("/images/background.jpg"));
                tmpData.setBtnLogin(btnLogin);
                tmpData.setBtnLogout(btnLogout);
                tmpData.setBtn();
            });
    }

    public void SetOnMouseClicked(MouseEvent event) throws IOException {

        boolean[] boolCheckMovie = new boolean[6];
        boolCheckMovie[0] = CheckMovie(m1ImageView, event);
        boolCheckMovie[1] = CheckMovie(m2ImageView, event);
        boolCheckMovie[2] = CheckMovie(m3ImageView, event);
        boolCheckMovie[3] = CheckMovie(m4ImageView, event);
        boolCheckMovie[4] = CheckMovie(m5ImageView, event);
        boolCheckMovie[5] = CheckMovie(m6ImageView, event);

        for(int i = 0; i < 7; i++){
            if(boolCheckMovie[i]){
                ((Node)event.getSource()).getScene().getWindow().hide();

                Stage stage = new Stage();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/selectTheatre.fxml"));

                stage.setTitle("Select Theatre");
                stage.setScene(new Scene(loader.load()));

                SelectTheatreController selectTheatreController = loader.getController();
                selectTheatreController.setMovie(m[i]);
                selectTheatreController.setTmpData(tmpData);

                stage.show();
                break;
            }
        }
    }
    @FXML
    private void SetOnEven(Event event) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/infoPage.fxml"));
        stage.setTitle("Info");
        stage.setScene(new Scene(loader.load()));
        stage.show();
    }

    @FXML
    public void BtnOnActionRegister(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/registerPage.fxml"));
        stage.setTitle("Register");
        stage.setScene(new Scene(loader.load()));
        stage.show();
    }

    @FXML
    public void BtnOnActionLoginHome(ActionEvent event) throws IOException {

        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/loginPage.fxml"));
        stage.setTitle("Log In");
        stage.setScene(new Scene(loader.load()));

        LoginController loginController = loader.getController();
        loginController.setTmpData(tmpData);

        stage.show();
    }

    @FXML
    public void BtnOnActionLogout(ActionEvent event){
        tmpData.setDisplayBtnLogin(1.0);
        tmpData.setDisplayBtnLogout(0.0);
        tmpData.setBtn();
         tmpData.setUserName("");
    }

    public void setTmpData(TmpData tmpData){
        this.tmpData = tmpData;
    }

    private boolean CheckMovie(ImageView imgMovie, MouseEvent event) {
        return imgMovie.equals(event.getSource());
    }
}