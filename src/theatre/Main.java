package theatre;

import filesCode.FileUser;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        createFile();

        Parent root = FXMLLoader.load(getClass().getResource("/view/homePage.fxml"));
        primaryStage.setTitle("Skizzjai Cineplex");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }

    private void createFile() throws IOException {
        File newDir = new File("files");
        newDir.mkdirs();
        File fileUser = new File("files/userDetails.csv");
        File fileBooking = new File("files/BookingData.csv");
        fileUser.createNewFile();
        fileBooking.createNewFile();

        FileUser createUser1 = new FileUser("Chinwong" , "Tangchringchai","chinwong.t@ku.th","skizzjai","12345678");
        int check;
        check = createUser1.checkUserAndPass();
        if (check != 1) {
            createUser1.save();
        }
        FileUser createUser2 = new FileUser("Tam","YaLay","shinnwong@hotmail.com","sernja","221213");
        check = createUser2.checkUserAndPass();
        if (check != 1) {
            createUser2.save();
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
