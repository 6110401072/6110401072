package theatre.selectChair;

public class Theatre {
    private String imgNormal;
    private String imgVIP;
    private String imgPremium;

    public Theatre(){
        this.imgNormal = "/images/theatre/normal.png";
        this.imgVIP = "/images/theatre/vip.png";
        this.imgPremium = "/images/theatre/premium.png";
    }

    public String getImgNormal() {
        return imgNormal;
    }
    public String getImgVIP() {
        return imgVIP;
    }
    public String getImgPremium() {
        return imgPremium;
    }

    public void setImgVIP(String imgVIP) {
        this.imgVIP = imgVIP;
    }
}
