package theatre.selectChair;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import theatre.TmpData;
import theatre.movie.Movie;

import java.io.IOException;

public class TicketController {

    private Movie movie;
    private TmpData tmpData;
    @FXML
    Text textMovieName, textTheatre, textTime, textPrice;

    @FXML public void initialize(){

        Platform.runLater(this::display);
    }

    private void display() {
        textMovieName.setText(tmpData.getMovieName());
        textTheatre.setText(tmpData.getTheatreName());
        textTime.setText(tmpData.getTime() + "");
        textPrice.setText(tmpData.getPrice() + "");
    }

    @FXML
    public void BtnOnActionBack(ActionEvent event) throws IOException {
        ((Node)event.getSource()).getScene().getWindow().hide();

        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/TheatreMovie.fxml"));
        stage.setTitle("Theatre");
        stage.setScene(new Scene(loader.load()));
        TheatreController theatreController = loader.getController();
        theatreController.setTmpData(tmpData);
        theatreController.setTheatre(tmpData.getTheatreName(), tmpData.getTmpPrice());
        theatreController.setMovie(movie);

        stage.show();
    }

    public void setTmpData(TmpData tmpData) {
        this.tmpData = tmpData;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
}
