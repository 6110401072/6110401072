package theatre.selectChair;

import filesCode.FileBookingData;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import theatre.TmpData;
import theatre.movie.Movie;

import java.io.IOException;

public class BuyTicketController{

    @FXML
    private Text textChairs, textPrice;
    private TmpData tmpData;
    private Movie movie;
    public void BtnOnActionBuy(ActionEvent event) throws IOException {
        ((Node)event.getSource()).getScene().getWindow().hide();

        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/ticketPage.fxml"));
        stage.setTitle("Ticket");
        stage.setScene(new Scene(loader.load()));

        TicketController ticketController = loader.getController();
        ticketController.setTmpData(tmpData);
        ticketController.setMovie(movie);
        FileBookingData fileBookingData = new FileBookingData(tmpData);
        fileBookingData.save();
        tmpData = fileBookingData.getTmpData();
        stage.show();
    }

    public void BtnOnActionBack(ActionEvent event) throws IOException {
        ((Node)event.getSource()).getScene().getWindow().hide();

        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/TheatreMovie.fxml"));
        stage.setTitle("Theatre");
        stage.setScene(new Scene(loader.load()));
        TheatreController theatreController = loader.getController();
        theatreController.setTmpData(tmpData);
        theatreController.setTheatre(tmpData.getTheatreName(), tmpData.getTmpPrice());
        theatreController.setMovie(movie);

        stage.show();
    }

    public void setTmpData(TmpData tmpData) {
        this.tmpData = tmpData;
    }

    public void display() {
        textChairs.setText(tmpData.getNameChair());
        textPrice.setText(tmpData.getPrice() + "");
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
}