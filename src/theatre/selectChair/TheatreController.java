package theatre.selectChair;

import filesCode.FileBookingData;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import theatre.TmpData;
import theatre.movie.Movie;
import theatre.selectTheatre.SelectTheatreController;

import java.io.IOException;

public class TheatreController extends Theatre{
    private boolean normal = false;
    private TmpData tmpData;
    private Movie movie;
    private int priceNormal, priceVIP, pricePremium;

    public TheatreController(){
        super();
    }

    @FXML
    Pane pane1;
    @FXML
    ImageView imgNormal, imgVip, imgPremium;
    @FXML
    Text textPriceNormal, textPricePremium, textPriceVip, textC/*text test*/;
    @FXML
    Button btnBack, buyTicket;

    @FXML
    public void initialize() {
        Platform.runLater(this::setChair);
    }

    public void setChair() {
        imgNormal.setImage(new Image(super.getImgNormal()));
        imgVip.setImage(new Image(super.getImgVIP()));
        imgPremium.setImage(new Image(super.getImgPremium()));

        ImageView[][] imgView = new ImageView[8][19];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 19; j++) {
                int finalI = i;
                int finalJ = j;
                boolean setImg = true;
                if (i == 6 || i == 3) {
                    imgView[i][j] = new ImageView();
                    imgView[i][j].setX(38 * j);
                    imgView[i][j].setY(66 * i);
                } else {
                    if (i > 6) {
                        if (j < 11) {
                            imgView[i][j] = new ImageView(new Image(super.getImgPremium()));
                            imgView[i][j].setFitWidth(44);
                            imgView[i][j].setX(66 * j);

                            imgView[i][j].setOnMouseClicked(event -> setImgOnMouseClick(imgView[finalI][finalJ], pricePremium, finalI, finalJ));
                            if (normal){
                                imgView[i][j].setOpacity(0.0);
                                imgView[i][j].setDisable(true);
                            }
                            setImg = false;
                        } else {
                            imgView[i][j] = new ImageView();
                        }
                    } else if (i > 3) {
                        imgView[i][j] = new ImageView(new Image(super.getImgVIP()));
                        imgView[i][j].setOnMouseClicked(event -> setImgOnMouseClick(imgView[finalI][finalJ], priceVIP, finalI, finalJ));
                    } else {
                        imgView[i][j] = new ImageView(new Image(super.getImgNormal()));
                        imgView[i][j].setOnMouseClicked(event -> setImgOnMouseClick(imgView[finalI][finalJ], priceNormal, finalI, finalJ));
                    }

                    checkStatusImgV(imgView[i][j], i, j);
                }
                if (setImg) {
                    imgView[i][j].setFitWidth(22);
                    imgView[i][j].setX(38 * j);
                }

                imgView[i][j].setY(35 * i);
                imgView[i][j].setFitHeight(28);
                pane1.getChildren().addAll(imgView[i][j]);
            }
        }
        setDisplayBtnBackClassBuy(imgView);
    }

    public void setDisplayBtnBackClassBuy(ImageView[][] imgView) {
        int count = 0;
        for (Integer intI : tmpData.getArrListPositionChairI()) {
            int j = tmpData.getArrListPositionChairJ().get(count);
            imgView[intI][j].setOpacity(0.5);
            count++;
        }

    }

    public void checkStatusImgV(ImageView imageView, int positionI, int positionJ) {
        FileBookingData fileBookingData = new FileBookingData(tmpData);
        fileBookingData.readBooking();
        tmpData = fileBookingData.getTmpData();
        int count = 0;
        for (Integer intI : tmpData.getOldPositionChairI()) {
            if ((intI == positionI) && (tmpData.getOldPositionChairJ().get(count) == positionJ)){
                imageView.setOpacity(0.5);
                imageView.setDisable(true);
            }
            count++;
        }
    }

    @FXML
    public void setImgOnMouseClick(ImageView imageView, int price, int i, int j){
        if (imageView.getOpacity() == 1.0){
            imageView.setOpacity(0.5);
            tmpData.setPrice(price);
            tmpData.addPositionChairI(i);
            tmpData.addPositionChairJ(j);
        }else {
            imageView.setOpacity(1.0);
            tmpData.setPrice(-price);
            int countArrList = 0;
            for (Integer integer : tmpData.getArrListPositionChairI()) {
                if ((integer == i) && (tmpData.getArrListPositionChairJ().get(countArrList) == j)){
                    tmpData.removePositionChairI(countArrList);
                    tmpData.removePositionChairJ(countArrList);
                    break;
                }
                countArrList++;
            }
        }

        if (tmpData.getNameChair().equals("")){
            buyTicket.setOpacity(0.0);
            buyTicket.setDisable(true);
        }else {
            buyTicket.setOpacity(1.0);
            buyTicket.setDisable(false);
        }
    }

    public void BtnOnActionBack(Event event) throws IOException {
        ((Node)event.getSource()).getScene().getWindow().hide();

        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/selectTheatre.fxml"));
        stage.setTitle("Select Theatre");
        stage.setScene(new Scene(loader.load()));

        SelectTheatreController selectTheatreController = loader.getController();
        tmpData.setPrice(-tmpData.getPrice());
        tmpData.clearPositionChair();
        tmpData.setTheatreName("");
        tmpData.setTime("");
        tmpData.clearPositionChairI();
        tmpData.clearPositionChairJ();
        tmpData.clearOldPositionChairI();
        tmpData.clearOldPositionChairJ();
        selectTheatreController.setMovie(movie);
        selectTheatreController.setTmpData(tmpData);

        stage.show();
    }

    public void setTheatre(String normal, int price) {
        tmpData.setTmpPrice(price);
        priceNormal = price;
        priceVIP = price + 50;
        pricePremium = price + 350;

        if (normal.equals("normal")) {
            priceVIP = priceNormal;
            this.normal = true;
            super.setImgVIP(super.getImgNormal());
            imgVip.setOpacity(0.0);
            imgPremium.setOpacity(0.0);
            textPriceNormal.setText("Normal " + priceNormal + " baht");
        } else  {
            textPriceNormal.setText("Normal " + priceNormal + " baht");
            textPriceVip.setText("VIP " + priceVIP + " baht");
            textPricePremium.setText("Premium " + pricePremium + " baht");
        }
    }

    public void setTmpData(TmpData tmpData){
        this.tmpData = tmpData;
    }

    @FXML
    public void BtnOnActionBuyTicket(Event event) throws IOException {
        ((Node)event.getSource()).getScene().getWindow().hide();
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/bookingPage.fxml"));
        stage.setTitle("Buy Ticket");
        stage.setScene(new Scene(loader.load()));

        BuyTicketController buyTicketController = loader.getController();
        buyTicketController.setTmpData(tmpData);
        buyTicketController.display();
        buyTicketController.setMovie(movie);

        stage.show();
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
}