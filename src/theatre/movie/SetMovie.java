package theatre.movie;

public class SetMovie {
    private Movie[] m = new Movie[6];

    public SetMovie(){
        m[0] = new Movie("Kingsman", "12/02/2015", "Action/Spy/Comedy", "129", "/images/poster/m1Kingsman.jpg",
                "A spy organization recruits an unrefined, but promising street kid into the agency's ultra-competitive training program, just as a global threat emerges from a twisted tech genius.",
                "/video/trailerKingsman.mp4");
        m[1] = new Movie("Avatar", "18/12/2009", "Action/Adventure/Fantasy/Sci-Fi", "166", "/images/poster/m2Avatar.jpg",
                "A paraplegic Marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
                "/video/trailerAvatar.mp4");
        m[2] = new Movie("Black Panther", "16/02/2018", "Action/Adventure/Sci-Fi", "134", "/images/poster/m3BlackPanther.jpg",
                "T'Challa, heir to the hidden but advanced kingdom of Wakanda, must step forward to lead his people into a new future and must confront a challenger from his country's past.",
                "/video/trailerBlackPanther.mp4");
        m[3] = new Movie("Jumanji", "20/12/2017", "Action/Adventure/Comedy", "119", "/images/poster/m4Jumanji.jpg",
                "Four teenagers are sucked into a magical video game, and the only way they can escape is to work together to finish the game.",
                "/video/trailerJumanji.mp4");
        m[4] = new Movie("Alita", "13/02/2019", "Action/Adventure/Sci-Fi", "122", "/images/poster/m5Alita.jpg",
                "A deactivated cyborg is revived, but cannot remember anything of her past life and goes on a quest to find out who she is.",
                "/video/trailerAlita.mp4");
        m[5] = new Movie("Captain Marvel", "08/03/2019", "Action/Adventure/Sci-Fi", "123", "/images/poster/m6CaptainMarvel.jpg",
                "Carol Danvers becomes one of the universe's most powerful heroes when Earth is caught in the middle of a galactic war between two alien races.",
                "/video/trailerCaptainMarvel.mp4");
    }

    public Movie[] getM() {
        return m;
    }
}
