package theatre.movie;

public class Movie {
    private String name;
    private String date;
    private String type;
    private String time;
    private String image;
    private String description;
    private String trailer;

    public Movie(String name, String date, String type, String time, String img, String description, String trailer) {
        this.name = name;
        this.date = date;
        this.type = type;
        this.time = time;
        this.image = img;
        this.description = description;
        this.trailer = trailer;
    }

    public String getName() {
        return name;
    }
    public String getDate() {
        return date;
    }
    public String getType() {
        return type;
    }
    public String getTime() {
        return time;
    }
    public String getImage() {
        return image;
    }
    public String getDescription() {
        return description;
    }
    public String getTrailer() {
        return trailer;
    }
}
