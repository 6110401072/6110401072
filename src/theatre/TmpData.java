package theatre;

import javafx.scene.control.Button;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class TmpData {
    private Button btnLogin, btnLogout;
    private double displayBtnLogin, displayBtnLogout;
    private int tmpPrice, price = 0;
    private List<Integer> arrListPositionChairI = new CopyOnWriteArrayList<>();
    private List<Integer> arrListPositionChairJ = new CopyOnWriteArrayList<>();
    private List<Integer> oldPositionChairI = new CopyOnWriteArrayList<>();
    private List<Integer> oldPositionChairJ = new CopyOnWriteArrayList<>();
    private String userName, movieName, theatreName,  time;

    public TmpData() {
        displayBtnLogin = 1.0;
        displayBtnLogout = 0.0;
    }

    public void setBtnLogin(Button btnLogin) {
        this.btnLogin = btnLogin;
    }

    public void setBtnLogout(Button btnLogout) {
        this.btnLogout = btnLogout;
    }

    public void setDisplayBtnLogin(double displayBtnLogin) {
        this.displayBtnLogin = displayBtnLogin;
    }

    public void setDisplayBtnLogout(double displayBtnLogout) {
        this.displayBtnLogout = displayBtnLogout;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setPrice(int price) {
        this.price += price;
    }

    public void setTmpPrice(int tmpPrice) {
        this.tmpPrice = tmpPrice;
    }

    public void addPositionChairI(int i) {
        this.arrListPositionChairI.add(i);
    }

    public void clearPositionChairI(){
        arrListPositionChairI.clear();
    }

    public void addPositionChairJ(int j) {
        this.arrListPositionChairJ.add(j);
    }

    public void clearPositionChairJ(){
        arrListPositionChairJ.clear();
    }

    public List<Integer> getArrListPositionChairI() {
        return arrListPositionChairI;
    }

    public List<Integer> getArrListPositionChairJ() {
        return arrListPositionChairJ;
    }

    public void removePositionChairI(int index){
        this.arrListPositionChairI.remove(index);
    }

    public void removePositionChairJ(int index){
        this.arrListPositionChairJ.remove(index);
    }

    public void clearPositionChair(){
        arrListPositionChairI.clear();
        arrListPositionChairJ.clear();
    }

    public String getNameChair(){
        int count  = 0;
        String nameChair = "";
        for (int i : arrListPositionChairI) {
            char c = 'A';
            int j = arrListPositionChairJ.get(count);
            if (i > 6){
                c += i - 2;
            } else if (i > 3){
                c += i - 1;
            } else {
                c += i;
            }
            nameChair += String.valueOf(c) + (j + 1) + " ";
            count++;
        }
        return nameChair;
    }

    public void addOldPositionChairI(int intI) {
        oldPositionChairI.add(intI);
    }

    public void clearOldPositionChairI(){
        oldPositionChairI.clear();
    }

    public void addOldPositionChairJ(int intJ) {
        oldPositionChairJ.add(intJ);
    }
    public void clearOldPositionChairJ(){
        oldPositionChairJ.clear();
    }

    public List<Integer> getOldPositionChairI() {
        return oldPositionChairI;
    }

    public List<Integer> getOldPositionChairJ() {
        return oldPositionChairJ;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getMovieName() {
        return movieName;
    }

    public int getPrice() {
        return price;
    }

    public int getTmpPrice() {
        return tmpPrice;
    }

    public String getTime() {
        return time;
    }

    public void setTheatreName(String theatreName) {
        this.theatreName = theatreName;
    }

    public String getTheatreName() {
        return theatreName;
    }

    public void setBtn() {
        btnLogin.setOpacity(displayBtnLogin);
        btnLogout.setOpacity(displayBtnLogout);

        if (displayBtnLogin == 1.0) {
            btnLogout.setDisable(true);
            btnLogin.setDisable(false);
        } else {
            btnLogin.setDisable(true);
            btnLogout.setDisable(false);
        }
    }

}
