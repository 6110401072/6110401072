package theatre.home;

import filesCode.FileUser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class RegisterController {
    @FXML
    TextField textFieldFirstName, textFieldLastName, textFieldEmail, textFieldUsername, textFieldPassword;
    @FXML
    Label labelErrorUsername, labelRegisterComplete;


    @FXML
    public void BtnOnAction(ActionEvent event) {
        FileUser fileUser = new FileUser(textFieldFirstName.getText(), textFieldLastName.getText(), textFieldEmail.getText(), textFieldUsername.getText(), textFieldPassword.getText());
        if (fileUser.registerCheck()) {
            labelErrorUsername.setOpacity(1.0);
        } else {
            fileUser.save();
            labelRegisterComplete.setText("Register complete!!\nPlease login");
        }
    }
}
