package theatre.home;

import filesCode.FileUser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import theatre.SelectMovieController;

public class LoginController extends SelectMovieController {
    @FXML
    TextField textFieldUser;
    @FXML
    PasswordField passwordFieldPassword;
    @FXML
    Label labelLogIncomplete;

    @FXML public void initialize(){
    }


    @FXML
    public void BtnOnActionLogIn(ActionEvent event){
        String username = textFieldUser.getText();
        String password = passwordFieldPassword.getText();

        FileUser fileUser = new FileUser(username, password);
        int check = fileUser.checkUserAndPass();
        if (check == 1){
            labelLogIncomplete.setText("Log in complete");
            super.tmpData.setDisplayBtnLogin(0.0);
            super.tmpData.setDisplayBtnLogout(1.0);
            super.tmpData.setBtn();
            super.tmpData.setUserName(textFieldUser.getText());

        } else if (check == 2){
            labelLogIncomplete.setText("password wrong\nplease try again");
        } else if (check == 3){
            labelLogIncomplete.setText("Username wrong\nplease try again");
        } else if (check == 4){
            labelLogIncomplete.setText("login fail \nplease try again");
        }
    }

}
