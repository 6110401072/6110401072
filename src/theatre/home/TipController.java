package theatre.home;

import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class TipController {
    @FXML
    Button btnBack;
    @FXML
    ImageView imgViBg, imvProfile;

    @FXML
    public void initialize(){
        Platform.runLater(() -> {
            imgViBg.setImage(new Image("/images/tip2.jpg"));
            imvProfile.setImage(new Image("/images/profile.jpg"));
        });
    }
    public void BtnOnActionBack(Event event) {
        ((Node)event.getSource()).getScene().getWindow().hide();
    }
}
