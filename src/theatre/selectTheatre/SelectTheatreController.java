package theatre.selectTheatre;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import theatre.SelectMovieController;
import theatre.TmpData;
import theatre.movie.Movie;
import theatre.selectChair.TheatreController;

import javax.print.DocFlavor;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;


public class SelectTheatreController {
    private Movie movie;

    @FXML
    Label textName, textDate, textType, textTime;
    @FXML
    Text description;
    @FXML
    ImageView movieImgV;
    @FXML
    Button btnTheatre1_1, btnTheatre1_2, btnTheatre2_1, btnTheatre2_2, btnTheatre3_1, btnTheatre3_2, btnTheatre4_1,btnTheatre4_2;
    @FXML
    MediaView mediaViewTrailer;

    private TmpData tmpData;
    private MediaPlayer mediaPlayer;

    @FXML public void initialize(){

        Platform.runLater(this::display);
    }

    private void display() {
        movieImgV.setImage(new Image(movie.getImage()));
        textName.setText(movie.getName());
        textDate.setText(movie.getDate());
        textType.setText(movie.getType());
        textTime.setText(movie.getTime() + " minutes");
        description.setText(movie.getDescription());
        mediaPlayer = new MediaPlayer(new Media(this.getClass().getResource(movie.getTrailer()).toExternalForm()));
        mediaPlayer.setAutoPlay(true);
        mediaViewTrailer.setMediaPlayer(mediaPlayer);
    }

    @FXML
    public void BtnOnAction1(ActionEvent event) throws IOException, URISyntaxException {
        ((Node)event.getSource()).getScene().getWindow().hide();

        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/TheatreMovie.fxml"));
        stage.setTitle("Theatre");
        stage.setScene(new Scene(loader.load()));
        TheatreController theatreController = loader.getController();
        theatreController.setMovie(movie);
        tmpData.setTheatreName("normal");
        findTimeBtnTheatre(event, btnTheatre1_1, btnTheatre1_2);
        theatreController.setTmpData(tmpData);
        theatreController.setTheatre("normal", 200);

        stage.show();
    }

    @FXML
    public void BtnOnAction2(ActionEvent event) throws IOException, URISyntaxException {
        ((Node)event.getSource()).getScene().getWindow().hide();

        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/TheatreMovie.fxml"));
        stage.setTitle("Theatre");
        stage.setScene(new Scene(loader.load()));
        TheatreController theatreController = loader.getController();
        theatreController.setMovie(movie);
        tmpData.setTheatreName("4K");
        findTimeBtnTheatre(event, btnTheatre2_1, btnTheatre2_2);
        theatreController.setTmpData(tmpData);
        theatreController.setTheatre("normal", 250);

        stage.show();
    }

    @FXML
    public void BtnOnAction3(ActionEvent event) throws IOException {
        ((Node)event.getSource()).getScene().getWindow().hide();

        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/TheatreMovie.fxml"));
        stage.setTitle("Theatre");
        stage.setScene(new Scene(loader.load()));
        TheatreController theatreController = loader.getController();
        theatreController.setMovie(movie);
        tmpData.setTheatreName("4K");
        findTimeBtnTheatre(event, btnTheatre3_1, btnTheatre3_2);
        theatreController.setTmpData(tmpData);
        theatreController.setTheatre("nonNormal", 250);

        stage.show();
    }

    @FXML
    public void BtnOnAction4(ActionEvent event) throws IOException {
        ((Node)event.getSource()).getScene().getWindow().hide();

        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/TheatreMovie.fxml"));
        stage.setTitle("Theatre");
        stage.setScene(new Scene(loader.load()));
        TheatreController theatreController = loader.getController();
        theatreController.setMovie(movie);
        tmpData.setTheatreName("3D");
        findTimeBtnTheatre(event, btnTheatre4_1, btnTheatre4_2);
        theatreController.setTmpData(tmpData);
        theatreController.setTheatre("nonNormal", 270);

        stage.show();
    }

    private void findTimeBtnTheatre(ActionEvent event, Button btnTheatre1, Button btnTheatre2) {
        mediaPlayer.stop();
        if (event.getSource().equals(btnTheatre1)){
            tmpData.setTime(btnTheatre1.getText());
        } else {
            tmpData.setTime(btnTheatre2.getText());
        }
    }

    @FXML
    public void BtnOnActionBack(Event event) throws IOException {
        mediaPlayer.stop();
        ((Node)event.getSource()).getScene().getWindow().hide();

        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/homePage.fxml"));
        stage.setTitle("Skizzjai Cineplex");
        stage.setScene(new Scene(loader.load()));

        SelectMovieController selectMovieController = loader.getController();
        tmpData.setMovieName("");
        selectMovieController.setTmpData(tmpData);

        stage.show();
    }

    public void setTmpData(TmpData tmpData) {
        this.tmpData = tmpData;
        this.tmpData.setMovieName(movie.getName());
    }
    public void setMovie(Movie movie){
        this.movie = movie;
    }
}
